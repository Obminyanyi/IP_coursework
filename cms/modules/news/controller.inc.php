<?php
class News_Controller
{
    public function EditAction($params)
    {
        $user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
        $user = (new Account_Model)->SelectByID($user_id);
        if ( $user_id && $user[0]['is_admin'] ) {
            $status = null;
            $view = new News_View();
            $model = new News_Model();
            $content = $view->Edit();
            $content->SetParam('item', $model->Select($params[0]));
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $row = $_POST;
                $row['text'] = base64_encode($row['text']);//кодування тексту новини (яка містить html, для форматування) в base64 для уникнення проблем при внесені в БД
                $model->UpdateById($params[0], $row);
                $content->SetParam('item', $model->Select($params[0]));
                $status = "Готово!";
            }
            return array(
                "PageTitle" => "Редагувати новину",
                "PageHeaderTitle" => "Редагувати новину",
                "Content" => $content->GetHTML(),
                "Status" => $status
            );
        } else {
            header('Location: /login/');
            return null;
        }
    }

    public function AddAction()
    {
        $user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
        $user = (new Account_Model)->SelectByID($user_id);
        if ( $user_id && $user[0]['is_admin'] ) {
            $status = null;
            $view = new News_View();
            $model = new News_Model();
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $row = $_POST;
                $row['text'] = base64_encode($row['text']);
                $model->Insert($row);
                $status = "Новину додано!";
            }
            return array(
                "PageTitle" => "Додати новину",
                "PageHeaderTitle" => "Додати новину",
                "Content" => $view->Add(),
                "Status" => $status
            );
        } else {
            header('Location: /login/');
            return null;
        }
    }

    public function IndexAction()
    {
        $view = new News_View();
        $model = new News_Model();
        $user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
        $user = (new Account_Model)->SelectByID($user_id);
        $content = $view->Index();
        $content->SetParam('News', $model->Select());
        if (isset($_SESSION['user_id'])) {
            $content->SetParam('Account', 'logged');
            $content->SetParam('is_admin', $user[0]['is_admin'] );
        }
        return array(
            "PageTitle" => "Новини",
            "PageHeaderTitle" => "Новини",
            "Content" => $content->GetHTML()
        );
    }

    public function DeleteAction($params)
    {
        if (isset($_SESSION['user_id'])) {
            $model = new News_Model();
            $model->DeleteById($params[0]);
            die(); //видалення за допомогою асинхронних запитів
        } else {
            header('Location: /login/');
            return null;
        }
    }
}