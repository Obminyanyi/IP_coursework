<?php
class News_Model
{
    public function DeleteById($id)
    {
        Core::$Db->DeleteById('news', 'id', $id);
    }

    public function Insert($row)
    {
        Core::$Db->Insert('news', $row);
    }

    public function Select($id = null)
    {
        $arr = null;
        if(!is_null($id)) {
            $arr = ['id ' => $id];
        }
        return Core::$Db->Select('news', ['id', 'title', 'text', 'date(date) as date'], $arr);
    }
    public  function UpdateById($id, $row)
    {
        Core::$Db->UpdateById('news', $row, 'id', $id);
    }
}