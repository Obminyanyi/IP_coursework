<?php
class Login_Controller
{
    public function IndexAction()
    {
        if(isset($_SESSION['user_id'])) //якщо користувач вже увійшов перенаправлення в його акаунт
        {
            header('Location: /account/');
        }
        $status = null;
        $view = new Login_View();
        $model = new Login_Model();
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $row = $_POST;
            $user = $model->Select($row); //пошук користувача в базі даних
            if(!empty($user)){ //якщо існує - створюємо сесію
                session_start();
                $_SESSION['user_id']=$user[0]['id'];
                header('Location: /');
            }
            else{
                $status = "Логін або пароль не вірні!";
            }
        }
        return array(
            "PageTitle" => "Вхід",
            "PageHeaderTitle" => "Вхід",
            "Content" => $view->Index(),
            "Status" => $status
        );
    }

}