<?php
class Core
{
    public static $Db;
    public static $IndexTPL;
    public static $_404TPL;
    public static function Init()
    {
        session_start();
        self::$Db = new Database("localhost", 'cms', 'root', ''); //Підключення до бд
        self::$IndexTPL = new Template("template/index.tpl"); //Підключення базової сторінки
        self::$_404TPL = new Template("template/404.tpl"); //Підключення 404
    }

    public static function Run()
    {
        $url = $_GET['url'];
        $parts = explode('/', $url);
        $className = ucfirst(array_shift($parts)) . '_Controller';
        $methodName = ucfirst(array_shift($parts)) . 'Action';
        if(isset($_SESSION['user_id'])) //встановлення статусу користувача (увійшов, чи ні)
        {
            self::$IndexTPL->SetParam('Account', 'logged');
        }else{
            self::$IndexTPL->SetParam('Account', 'out');
        }
        if ($className == '_Controller') { //перенаправлення на домашню сторінку при зверненні безпосередньо до сайту
            $className = 'Home'.$className;
        }
        if (class_exists($className)) {
            $moduleObject = new $className();
            if (method_exists($moduleObject, $methodName)) {
                $params = $moduleObject->$methodName($parts);
                self::$IndexTPL->SetParams($params);
            } else if ($methodName == 'Action') { //якщо дія не була задана (було введено наприклад site.ua/news) перенаправлення на дію "Index"
                $methodName = 'IndexAction';
                $params = $moduleObject->$methodName($parts);
                self::$IndexTPL->SetParams($params);
            } else {
                self::_404();
                return;
            }
        } else {
            self::_404();
            return;
        }
        self::$IndexTPL->Display();
    }

    public static function _404(){
        self::$_404TPL->SetParam('PageTitle', "404!");
        self::$_404TPL->SetParam('PageHeaderTitle', "Сторінку не знайдено!");
        self::$_404TPL->Display();
    }
}