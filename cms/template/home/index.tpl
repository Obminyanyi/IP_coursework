<p><b><a href="">«Хіт FM»</a></b> — найбільша радіомережа України. Музичний формат — європейська та вітчизняна поп-музика від 90-х років до сьогодення. Розпочала мовлення 28 серпня 1999 року, з 28 вересня 2001 року — повноцінне мовлення.</p>
<p style="margin-left:0in; margin-right:0in; text-align:center">
	<span style="font-size:12pt">
		<span style="font-family:&quot;Times New Roman&quot;,serif">
			<a href="" style="color:blue; text-decoration:underline">
				<img alt="" id="Рисунок_x0020_14" src=".\template\resource\files\images\hitfmlogo.png" style="height:141pt; width:187.5pt" />
			</a>&nbsp; &nbsp; &nbsp;
			<a href="" style="color:blue; text-decoration:underline">
				<img alt="" src=".\template\resource\files\images\c175.png" style="height:188px; width:250px" />
			</a>&nbsp; &nbsp; &nbsp;
			<a href="" style="color:blue; text-decoration:underline">
				<img alt="" id="Рисунок_x0020_13" src=".\template\resource\files\images\imghit.png" style="height:149.25pt; width:187.5pt" />
			</a>&nbsp; &nbsp; &nbsp;
		</span>
	</span>
</p>

<h4 style="text-align:center">Ведучі ранкового шоу Хеппі Ранок</h4>

<p style="margin-left:0in; margin-right:0in; text-align:center">
	<span style="font-size:12pt">
		<span style="font-family:&quot;Times New Roman&quot;,serif">
				<a href="https://www.hitfm.ua/dj/21-mykyta-shevchuk.html" title="Микита Шевчук"><img alt="Микита Шевчук" id="Рисунок_x0020_14" src=".\template\resource\files\images\v1.jpg" style="height:141pt; width:187.5pt"/></a>
			&nbsp; &nbsp; &nbsp;
				<a href="https://www.hitfm.ua/dj/22-julia-karpova.html" title="Юля Карпова"><img alt="Юля Карпова" src=".\template\resource\files\images\v2.jpg" style="height:188px; width:250px" /></a>
		</span>
	</span>
</p>


<h4 style="text-align:center">Ведучі ефіру</h4>
<p style="margin-left:0in; margin-right:0in; text-align:center">
	<span style="font-size:12pt">
		<span style="font-family:&quot;Times New Roman&quot;,serif">
				<a href="https://www.hitfm.ua/dj/10-ania-lisovska.html" title ="Аня Лісовська" ><img alt="Аня Лісовська" id="Рисунок_x0020_14" src=".\template\resource\files\images\v3.jpg" style="height:141pt; width:187.5pt" /></a>
			&nbsp; &nbsp; &nbsp;
				<a href="https://www.hitfm.ua/dj/20-denys-zhupnyk.html" title ="Денис Жупник" ><img alt="Денис Жупник" src=".\template\resource\files\images\v4.jpg" style="height:188px; width:250px" /></a>
			&nbsp; &nbsp; &nbsp;
				<a href="https://www.hitfm.ua/dj/23-simona-soloduha.html" title ="Сімона Солодуха" ><img alt="Сімона Солодуха" id="Рисунок_x0020_13" src=".\template\resource\files\images\v5.jpg" style="height:149.25pt; width:187.5pt" /></a>
		</span>
	</span>
</p>


<blockquote font-size: 14px; font-weight: bold;">
    Радіо: засіб масової інформації, за яким ніколи не показують старих фільмів.
</blockquote>

