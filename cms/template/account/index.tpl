<form method="post">
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" name="name" id="name" value="<?=$item[0]['name']?>" required>
        <label class="mdl-textfield__label" for="name">Ваше ім'я</label>
    </div>
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" name="email" id="email" value="<?=$item[0]['email']?>" required>
        <label class="mdl-textfield__label" for="email">E-mail</label>
    </div>
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="password" name="password" id="password" required>
        <label class="mdl-textfield__label" for="password">Пароль</label>
    </div>
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="password" name="confirm_password" id="confirm_password" required>
        <label class="mdl-textfield__label" for="confirm_password">Повторіть пароль</label>
    </div>
    <br>
    <br>
    <br>
    <input type="submit" value="Зберегти зміни" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored btn_size"/>
</form>

<button id="show-dialog" type="button" class="mdl-button mdl-button--raised mdl-js-button dialog-button mdl-color--red-600">Видалити аккаунт</button>
<dialog class="mdl-dialog">
    <h4 class="mdl-dialog__title">Видалити акаунт?</h4>
    <div class="mdl-dialog__content">
        <p>
            Ви справді впевнені, що хочете видалити свій акаунт?
        </p>
    </div>
    <div class="mdl-dialog__actions">
        <button type="button" class="mdl-button mdl-color--teal-500 close">Відмінити</button>
        <button type="button" class="mdl-button mdl-color--red-600 confirm">Видалити</button>
    </div>
</dialog>