
////////// валідація пароля
password_match_check();
function password_match_check() {
    var password = document.getElementById("password")
        , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Паролі не співпадать");
        } else {
            confirm_password.setCustomValidity('');
        }
        if(password.value.length < 6)
        {
            password.setCustomValidity("Пароль повинен містити як мінімум 6 символів");
        }else if(password.value.length > 25)
        {
            password.setCustomValidity("Пароль може містити максимум 25 символів");
        } else {
            password.setCustomValidity('');
        }
    }
try {
    password.onkeyup = validatePassword;
    confirm_password.onkeyup = validatePassword;
}
catch (e){}
}


///////////// діалогове вікно видалнння акаунта
try{
var dialog = document.querySelector('dialog');
var showDialogButton = document.querySelector('#show-dialog');
if (! dialog.showModal) {
    dialogPolyfill.registerDialog(dialog);
}
showDialogButton.addEventListener('click', function() {
    dialog.showModal();
});
dialog.querySelector('.close').addEventListener('click', function() {
    dialog.close();
});
dialog.querySelector('.confirm').addEventListener('click', function() {
    document.location.href = "/account/delete";
});}
catch (e){}



////////////// текстовий редактор CKEDITOR
try {
    CKEDITOR.replace('text');
}
catch (e){

}

/////////// dropdown меню акаунт
$(document).ready(function(){

    $.easing.def = "easeInOutQuad";
    $('div.button a').click(function(e){
        var dropDown = $(this).parent().next();
        $('.dropdown').not(dropDown).slideUp(350);
        dropDown.slideToggle(320);
        e.preventDefault();
    })
});

/////// Дозволяти грати лише одній пісні в тей же час
$(function(){
    $("audio").on("play", function() {
        $("audio").not(this).each(function(index, audio) {
            this.currentTime = 0;
            audio.pause();
        });
    });
});

/////////автоматичний програх наступної пісні
var audios = $('audio'),
    len = audios.length,
    i = 0;
audios.bind('ended', function(e){
    i++;
    audios[i].play();
});


//////////// приклад використання ajax, видалення пісень

function ajax_func(btn) {
    $.ajax(btn.value)
        .done(function() {
            var tab = $(btn).parent().parent().parent().parent().parent();
            (tab).next('hr').remove();
            tab.remove();
        })
        .fail(function() {
        })
        .always(function() {
        });
}