<form enctype="multipart/form-data" method="post">
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" name="artist" id="artist" required>
        <label class="mdl-textfield__label" for="artist">Виконавець</label>
    </div>
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" name="title" id="title" required>
        <label class="mdl-textfield__label" for="title">Назва пісні</label>
    </div>
    <br>
    <input class="mdl-textfield__input" name="audio" accept="audio/*" type="file" />
    <br>
    <br>
    <br>
    <input type="submit" value="Завантажити" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored btn_size"/>
</form>
