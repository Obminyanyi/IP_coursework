<form method="post">
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" name="name" id="name" required>
        <label class="mdl-textfield__label" for="name">Ваше ім'я</label>
    </div>
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" name="email" id="email" required>
        <label class="mdl-textfield__label" for="email">E-mail</label>
    </div>
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="password" name="password" id="password" required>
        <label class="mdl-textfield__label" for="password">Пароль</label>
    </div>
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="password" name="confirm_password" id="confirm_password" required>
        <label class="mdl-textfield__label" for="confirm_password">Повторіть пароль</label>
    </div>
    <br>
    <br>
    <br>
    <input type="submit" value="Зареєструватися" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored btn_size"/>
</form>